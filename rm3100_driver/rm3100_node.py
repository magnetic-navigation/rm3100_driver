# BSD 3-Clause License

# Copyright (c) 2022, Avular B.V.
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:

# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.

# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.

# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import math
import smbus
import struct
import numpy as np

import rclpy
from rclpy.node import Node
from sensor_msgs.msg import MagneticField #the message for magnetic field data
# yellow arrow magnetometer
calibrated_coefficients = ([0.0139373886623529,	-0.000278294282953477,	-0.000532149820481804],
                            [-7.05410926524239e-05,	0.0143717480438287,	-0.000108109358331106],
                            [0.000472459860259188,	6.94965900374803e-05,	0.0138137338302984])



offsets = [7.83871376840888, -7.86414905956548, -7.13783224871931]

bus = smbus.SMBus(1)
address = 0x20 # address 21 for the other magnetometer; 20 is for the yellow arrow magnetometer
rm3100_POLL = 0x00
rm3100_CMM = 0x01
rm3100_Mx2w = 0x24


def recast24to32(byte0, byte1, byte2):
    # pack 24 bits (3 bytes) into 32 bits byte-type
    b24 = struct.pack('xBBB', byte0, byte1, byte2)

    # unpack to unsigned long integer
    uL = struct.unpack('>L', b24)[0]

    # this is for 2's complement signed numbers -
    #   if negative assign sign bits for 32 bit case
    if (uL & 0x00800000):
        uL = uL | 0xFF000000

    # repack as 32 bit unsigned long byte-type
    packed = struct.pack('>L', uL)
    # unpack as 32 bit signed long integer
    unpacked = struct.unpack('>l', packed)[0]

    return unpacked

def read_rm3100():
    
    bus.write_byte_data(address, rm3100_POLL, 0x00)

    bus.write_byte_data(address, rm3100_CMM, 0b01111001)

    x = []
    y = []
    z = []



    for j in range(100):
        # read 9 bytes of data,  3 bytes for each axis
        raw = bus.read_i2c_block_data(address, rm3100_Mx2w, 9)
        values = []
        for i in range(0, 9, 3):
            data = float(recast24to32(raw[i], raw[i+1], raw[i+2]))
            values.append(data)
        x.append(values[0])
        y.append(values[1])
        z.append(values[2])
    x_sum = 0
    y_sum = 0
    z_sum = 0

    for i in range(100):
        x_sum = x_sum+x[i]
        y_sum = y_sum+y[i]
        z_sum = z_sum+z[i]
    x_sum = x_sum/100
    y_sum = y_sum/100
    z_sum = z_sum/100

    magnetic_field_values = np.dot(calibrated_coefficients, [x_sum, y_sum, z_sum])

    magnetic_field_values = magnetic_field_values - offsets

    print(magnetic_field_values)

    return magnetic_field_values


class RM3100Node(Node):
    def __init__(self) -> None:
        super().__init__('rm3100')
        self.publisher_ = self.create_publisher(MagneticField, 'mag_vals', 10)
        timer_period = 0.2  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback)

    def timer_callback(self):
        mag_msg=MagneticField()
        values = read_rm3100()
        # assign xyz values to the message here
        # the magnetic field values are in microTesla        
        mag_msg.magnetic_field.x=values[0]
        mag_msg.magnetic_field.y=values[1]
        mag_msg.magnetic_field.z=values[2]
        # publish the values
        self.publisher_.publish(mag_msg)
        
        self.get_logger().info("x = {:.2}, y = {:.2}, z = {:.2}, abs = {:.2}".format(
            values[0], values[1], values[2],
            math.sqrt(values[0]**2 + values[1]**2 + values[2]**2)))

        #heading = math.atan2(values[0], values[1])*180/math.pi
        #self.get_logger().info("Heading = {:.2f}".format(heading))


def main(args=None):
    rclpy.init(args=args)
    rm3100 = RM3100Node()
    rm3100.get_logger().info('Starting RM3100 driver node')

    rclpy.spin(rm3100)

    rm3100.destroy_node()
    rclpy.shutdown()
    # also close the bus
    bus.close()


if __name__ == '__main__':
    main()
