# BSD 3-Clause License

# Copyright (c) 2022, Avular B.V.
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:

# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.

# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.

# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import math
import smbus
import struct
import csv

import rclpy
from rclpy.node import Node

filename = ""

def recast24to32(byte0, byte1, byte2):
    # pack 24 bits (3 bytes) into 32 bits byte-type
    b24 = struct.pack('xBBB', byte0, byte1, byte2)

    # unpack to unsigned long integer
    uL = struct.unpack('>L', b24)[0]

    # this is for 2's complement signed numbers -
    #   if negative assign sign bits for 32 bit case
    if (uL & 0x00800000):
        uL = uL | 0xFF000000

    # repack as 32 bit unsigned long byte-type
    packed = struct.pack('>L', uL)
    # unpack as 32 bit signed long integer
    unpacked = struct.unpack('>l', packed)[0]

    return unpacked


def read_rm3100():
    bus = smbus.SMBus(3)
    address = 0x20
    rm3100_POLL = 0x00
    rm3100_CMM = 0x01

    rm3100_Mx2w = 0x24

    bus.write_byte_data(address, rm3100_POLL, 0x00)

    bus.write_byte_data(address, rm3100_CMM, 0b01111001)

    x = []
    y = []
    z = []

    #x_coeff = [-14.45, 2219.57]
    #y_coeff = [-14.20, 2247.13]
    #z_coeff = [14.02, -20454.22]
    
    x_coeff=[1, 0]
    y_coeff=[1, 0]
    z_coeff=[1,0]

    for j in range(100):
        # read 9 bytes of data,  3 bytes for each axis
        raw = bus.read_i2c_block_data(address, rm3100_Mx2w, 9)
        values = []
        for i in range(0, 9, 3):
            data = float(recast24to32(raw[i], raw[i+1], raw[i+2]))
            values.append(data)
        x.append(values[0])
        y.append(values[1])
        z.append(values[2])
    x_sum = 0
    y_sum = 0
    z_sum = 0

    for i in range(100):
        x_sum = x_sum+x[i]
        y_sum = y_sum+y[i]
        z_sum = z_sum+z[i]
    x_sum = x_sum/100
    y_sum = y_sum/100
    z_sum = z_sum/100

    values = [(x_sum*x_coeff[0] + x_coeff[1]),
              (y_sum*y_coeff[0] + y_coeff[1]),
              (z_sum*z_coeff[0] + z_coeff[1])]

    return values


def write_data_to_csv(values):
    # WRITE THE X,Y,Z VALUES TO EACH ROW OF CSV FILES
    # OPEN THE FILE, SET THE PROPER PATH - SAME FOLDER AS WHERE THIS FILE IS
    with open(filename, 'a', encoding='UTF8', newline='') as f:
        datacsv = csv.writer(f)
        datacsv.writerow(values)


class RM3100Node(Node):
    def __init__(self) -> None:
        super().__init__('rm3100')
        timer_period = 0.2  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback)

    def timer_callback(self):
        values = read_rm3100()
        write_data_to_csv(values)
        self.get_logger().info("x = {:.2}, y = {:.2}, z = {:.2}, abs = {:.2}".format(
            values[0], values[1], values[2],
            math.sqrt(values[0]**2 + values[1]**2 + values[2]**2)))

        heading = math.atan2(values[0], values[1])*180/math.pi
        self.get_logger().info("Heading = {:.2f}".format(heading))


def main(args=None):
    rclpy.init(args=args)

    print("Enter the file name")
    fname = input()

    global filename
    filename = fname + ".csv"

    rm3100 = RM3100Node()
    rm3100.get_logger().info('Starting RM3100 driver node')

    rclpy.spin(rm3100)

    rm3100.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
