from setuptools import setup

package_name = 'rm3100_driver'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='Pasquale van Heumen',
    maintainer_email='p.heumen@avular.com',
    description='RM3100 Magnetic Sensor driver package',
    license='MIT',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'rm3100_node = rm3100_driver.rm3100_node:main'
        ],
    },
)
